package utils

import "github.com/go-rod/rod"

type Browser struct {
	Browser *rod.Browser
	Alive   bool
}
