package utils

import "github.com/go-rod/rod/lib/launcher"

type RodProcess struct {
	Alive           bool
	ProcessLocation *launcher.Launcher
	ProcessURL      string
}

func NewRodProcess() *RodProcess {

	wg := RodProcess{Alive: false}

	return &wg
}
