package utils

import "github.com/go-rod/rod"

type BrowserTab struct {
	ParentBrowser *Browser
	Page          rod.Page
}
