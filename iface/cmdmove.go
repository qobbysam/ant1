package iface

import "context"

// Interface Definitions for A task, Station Task, Worker Task

type CmdMoveInterface interface {
	StartConsuming(ctx context.Context) (bool, error)
	StopConsuming(ctx context.Context) error
}
