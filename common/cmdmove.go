package common

import (
	"context"
	"errors"
	"sync"
)

type RegisteredWorkerTaskNames struct {
	sync.RWMutex
	items []string
}

type CmdMove struct {
	//	Station *Station
	//	Worker  *Worker

	//retry    bool
	//StopChan      chan struct{}
	retry         bool
	retryFunc     func(chan int)
	retryStopChan chan int
	stopChan      chan int //RegisteredWorkerTaskNames RegisteredWorkerTaskNames

	//	StationFuncs sync.Map
	//	WorkerFuncs  sync.Map
}

// func (cm *CmdMove) StartConsuming(consumerTag string, concurrency int, taskProcessor iface.TaskProcessor) {
// 	if b.retryFunc == nil {
// 		b.retryFunc = retry.Closure()
// 	}

// }

func NewCmdMove() *CmdMove {

	wg := CmdMove{}

	wg.stopChan = make(chan int)
	wg.retryStopChan = make(chan int)

	return &wg

}

func (b *CmdMove) GetRetry(ctx context.Context) bool {
	return b.retry
}

// GetRetryFunc ...
func (b *CmdMove) GetRetryFunc() func(chan int) {
	return b.retryFunc
}

// GetRetryStopChan ...
func (b *CmdMove) GetRetryStopChan() chan int {
	return b.retryStopChan
}

// GetStopChan ...
func (b *CmdMove) GetStopChan() chan int {
	return b.stopChan
}

func (cm *CmdMove) StartConsuming(ctx context.Context) error {

	//	err := cm.Worker.StartConsuming(ctx)

	//	go Watch(cm.Station.Coms, ctx)

	return errors.New("not implemented")
}

func (cm *CmdMove) StopConsuming(ctx context.Context) error {

	return nil
}

// func NewCmdMove() *CmdMove {

// 	station := new(Station)
// 	worker := new(Worker)

// 	return &CmdMove{Station: station, Worker: worker}
// }
