package common

import (
	"context"
	"local/ant1/pawnserver/iface"
	utils "local/ant1/pawnserver/utils"
)

//type AllBrowsers []*utils.Browser

//type AllTabs []*utils.BrowserTab

type RegisteredMoves []iface.CmdMoveInterface

type Engine struct {
	LocalRod        *utils.RodProcess
	CurrentBrowser  *utils.Browser
	CurrentTab      *utils.BrowserTab
	AllBrowser      utils.AllBrowsers
	AllTabs         utils.AllTabs
	RegisteredMoves RegisteredMoves
	CloseChan       chan struct{}
}

func NewEngine(ctx context.Context) *Engine {

	//get gaintconfig from ctx

	wg := Engine{}

	localrod := utils.NewRodProcess()
	wg.CloseChan = make(chan struct{})

	wg.LocalRod = localrod

	return &wg

}

func AddEngineToCtx(en Engine, ctx context.Context) context.Context {

	//engine_str := "engine"

	new_ctx := context.WithValue(ctx, EngineCtxString, en)
	//ctx.Value(engine_str) = en

	return new_ctx

}
