package common

type SuperConfig struct {
	RedisConfig *RedisConfig
}

type RedisConfig struct {
	//	host, password, socketPath string, db int

	Host       string
	Password   string
	socketPath string
	db         int
}
