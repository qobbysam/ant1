package cmdpointermove

import (
	"context"
	"local/ant1/pawnserver/common"
	"local/ant1/pawnserver/iface"
)

type CmdPointerMove struct {
	common.CmdMove

	Station *PointerStation
	Worker  *PointerWorker

	Ctx context.Context

	//Retry bool
	//Coms string

	//WorkerFuncs map[type]type

}

// func (cm *CmdPointerMove) GetRetry(ctx context.Context) bool {

// 	return cm.Retry

// }

func NewCmdPointerMove(ctx context.Context) (iface.CmdMoveInterface, error) {

	outcmd := CmdPointerMove{}

	cmdglobal := common.NewCmdMove()

	station, err := NewPointerStation(ctx)

	if err != nil {
		//faied to create new pointerstation
		return nil, err
	}

	worker, err := NewPointerWorker(ctx)

	if err != nil {

		//failed to create worker

		ctx.Err()

		return nil, err
	}

	outcmd.CmdMove = *cmdglobal
	outcmd.Ctx = ctx
	outcmd.Station = station
	outcmd.Worker = worker
	//outcmd.Retry = true

	return &outcmd, err
}

func (cm *CmdPointerMove) StartConsuming(ctx context.Context) (bool, error) {

	errchan := make(chan error)

	err := cm.Worker.StartConsumingServer(ctx)

	if err != nil {

		errchan <- err
	}

	go func() {

		for {
			select {

			// case in := <-cm.CmdMove.GetStopChan():
			// 	ctx.Done()

			case <-errchan:
				ctx.Done()

			case <-ctx.Done():

				//cm.Retry = false

				break

				//return false, <-errchan

			}
		}

	}()

	//return cm.GetRetry(ctx), <-errchan

	//return true, <-errchan

	return cm.GetRetry(ctx), <-errchan
}

func (cm *CmdPointerMove) StopConsuming(ctx context.Context) error {

	return nil
}
