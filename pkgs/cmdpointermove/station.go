package cmdpointermove

import (
	"context"
	"errors"
	"local/ant1/pawnserver/utils"
)

type PointerStation struct {
	localRod     *utils.RodProcess //Rod Process
	localBrowser *utils.Browser    //Browser
	CurrentTab   *utils.BrowserTab //BrowserTab
	AllBrowsers  utils.AllBrowsers
	AllTabs      utils.AllTabs
}

func NewPointerStation(ctx context.Context) (*PointerStation, error) {
	ps := PointerStation{}

	// value := ctx.Value(common.EngineCtxString)

	// engine, ok := value.(*common.Engine)

	// if !ok {
	// 	// engine has disappeared from context

	// 	return nil, errors.New("engine has disappeared from ctxt")
	// }

	// ps.localRod = engine.LocalRod
	// ps.localBrowser = engine.CurrentBrowser
	// ps.CurrentTab = engine.CurrentTab
	// ps.AllTabs = engine.AllTabs
	// ps.AllBrowsers = engine.AllBrowser

	return &ps, nil
}

func (ps *PointerStation) TurnOn(ctx context.Context) error {

	_, err := ps.localRod.ProcessLocation.Launch()

	if err != nil {

		//errors.Unwrap(err)

		return errors.New("Local Rod Process could not start")
	}

	return nil
}

func (ps *PointerStation) TurnOff(ctx context.Context) error {

	ps.localRod.ProcessLocation.Kill()

	//rod is off

	return nil
}
