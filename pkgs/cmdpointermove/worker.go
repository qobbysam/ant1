package cmdpointermove

import (
	"context"
	"fmt"
	"local/ant1/pawnserver/common"
)

type PointerWorker struct {
	*common.WorkerServer

	ServerUrl   string
	Subject     string
	ConsumerTag string

	//funs

}

func NewPointerWorker(ctx context.Context) (*PointerWorker, error) {

	queuename := "somename"
	ConsumerTag := "pointerconsumer"

	workerserver := common.NewWorker(ctx, queuename, ConsumerTag)

	wg := PointerWorker{
		WorkerServer: workerserver,
	}

	return &wg, nil

}

func (pw *PointerWorker) StartConsumingServer(ctx context.Context) error {

	env := make(chan struct{})

	defer close(env)

	//engine := ctx.Value("engine")

	//value, ok := engine.(*common.Engine)

	// if !ok {

	// 	env <- struct{}{}

	// }

	// fmt.Sprint(value)
	// fmt.Sprint(ok)

	//errChan := make(chan error)

	err := pw.WorkerServer.Worker.Launch()

	go func() {

		for {

			select {
			//case <-ctx.Done():
			//stop consuming here
			case <-env:
				fmt.Println("error Received")
				ctx.Done()
			}
		}

	}()

	return err
}
