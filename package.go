package main

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"sync"

	movecmd "local/ant1/pawnserver/iface"
	cmdpointermove "local/ant1/pawnserver/pkgs/cmdpointermove"
	//	"github.com/RichardKnop/machinery/v1/tasks"
)

func main() {

	ctx := context.Background()

	pw, err := NewPawnServer(ctx)

	if err != nil {

		fmt.Println(err)
	}

	err = pw.StartPawnServer(ctx)

	if err != nil {

		fmt.Println(err)
	}
}

type PawnServer struct {
	Name        string
	SuperCtx    context.Context
	RegistedCmd *sync.Map
	MainCmd     movecmd.CmdMoveInterface

	Retry bool
}

func NewPawnServer(ctx context.Context) (*PawnServer, error) {

	wg := PawnServer{}

	pointercmd, err := cmdpointermove.NewCmdPointerMove(ctx)

	if err != nil {

		return nil, err
	}
	wg.MainCmd = pointercmd

	return &wg, err
}

func (pw *PawnServer) StartPawnServer(ctx context.Context) error {
	// collect all config files

	//create register cmds here

	//create Engine

	//start main cmd here

	//watch all cmds

	retry, err := pw.MainCmd.StartConsuming(ctx)

	if !retry {

		ctx.Done()

		return err
	}
	return err

}

func (server *PawnServer) GetRetry(ctx context.Context) bool {

	return server.GetRetry(ctx)

}

func (server *PawnServer) RegisterTasks(namedCMDStructs map[string]interface{}) error {
	for _, cmd := range namedCMDStructs {
		if err := ValidateCMD(cmd); err != nil {
			return err
		}
	}
	for k, v := range namedCMDStructs {
		server.RegistedCmd.Store(k, v)
	}

	//server.broker.SetRegisteredTaskNames(server.GetRegisteredTaskNames())
	return nil
}

func ValidateCMD(cmd interface{}) error {
	v := reflect.ValueOf(cmd)
	t := v.Type()

	// Task must be a function
	if t.Kind() != reflect.Struct {
		return errors.New("cmds must be structs")
	}

	// Task must return at least a single value
	// if t.NumOut() < 1 {
	// 	return errors.New("cmds must return one value")
	// }

	// // Last return value must be error
	// lastReturnType := t.Out(t.NumOut() - 1)
	// errorInterface := reflect.TypeOf((*error)(nil)).Elem()
	// if !lastReturnType.Implements(errorInterface) {
	// 	return errors.New("last return value must be error")
	// }

	return nil
}
